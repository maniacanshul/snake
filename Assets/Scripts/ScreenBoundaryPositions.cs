﻿public struct ScreenBoundaryPositions
{
	public float screenOutTop;
	public float screenOutBottom;
	public float screenOutLeft;
	public float screenOutRight;
}
