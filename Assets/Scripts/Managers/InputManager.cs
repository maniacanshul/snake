﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
	public KeyCode up;
	public KeyCode down;
	public KeyCode left;
	public KeyCode right;

	/// <summary>
	/// Delegate and event system is used to fire events to all the subscribers listening for player input whenever a player input is recorded.
	/// </summary>
	public delegate void onInputRecieved(INPUT_DIRECTION dir);
	public event onInputRecieved OnInputRecievedEvent;

	private bool isInitialized;
	private INPUT_DIRECTION direction;

	/// <summary>
	/// Used to initialize sub systems from main game manager in the order of requirenemnt.
	/// </summary>
	public void Initialize()
	{
		isInitialized = true;
		direction = INPUT_DIRECTION.NONE;
	}


	/// <summary>
	/// Record input and broadcast using the event with the details of direction only if an input is recorded.
	/// </summary>
	private void Update()
	{
		if (!isInitialized)
		{
			return;
		}

		if (Input.GetKeyDown(up))
		{
			direction = INPUT_DIRECTION.UP;
		}
		else if (Input.GetKeyDown(down))
		{
			direction = INPUT_DIRECTION.DOWN;
		}
		else if (Input.GetKeyDown(left))
		{
			direction = INPUT_DIRECTION.LEFT;
		}
		else if (Input.GetKeyDown(right))
		{
			direction = INPUT_DIRECTION.RIGHT;
		}
		else
		{
			direction = INPUT_DIRECTION.NONE;
		}

		if (direction != INPUT_DIRECTION.NONE)
		{
			if (OnInputRecievedEvent != null)
			{
				OnInputRecievedEvent(direction);
			}
		}
	}
}
