﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	public GameObject playButton;
	public GameObject restartButton;
	public Text scoreText;
	public Text timeText;


	/// <summary>
	/// Show or hide play button.
	/// </summary>
	/// <param name="enable">Decides whether to show or hide the play button.</param>
	public void EnablePlayButton(bool enable)
	{
		playButton.SetActive(enable);
	}

	/// <summary>
	/// Show or hide restart button.
	/// </summary>
	/// <param name="enable">Decides whether to show or hide the play button.</param>
	public void EnableRestartButton(bool enable)
	{
		restartButton.SetActive(enable);
	}

	/// <summary>
	/// Updates the UI with the updated score whenever the score is changed.
	/// </summary>
	/// <param name="score">The score value.</param>
	public void UpdateScoreText(int score)
	{
		scoreText.text = score.ToString();
	}

	/// <summary>
	/// Updates the UI with the updated time in seconds whenever the time is updated.
	/// </summary>
	/// <param name="time">The time value.</param>
	public void UpdateTimeText(int time)
	{
		timeText.text = time.ToString();
	}
}
