﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	[Header("Managers")]
	public PoolManager poolManager;
	public UIManager uiManager;
	public InputManager inputManager;
	public CollectibleManager collectableManager;
	public PlayerManager playerManager;

	[Space(20)]
	public Camera mainCamera;

	private int score;
	private int time;
	private Coroutine timeCoroutine;

	private ScreenBoundaryPositions screenBoundaryPositions;


	/// <summary>
	/// Calculate screen boundaries on all 4 sides.
	/// </summary>
	private void CalculateScreenOutPositions()
	{
		screenBoundaryPositions.screenOutTop = mainCamera.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y;
		screenBoundaryPositions.screenOutBottom = mainCamera.ScreenToWorldPoint(new Vector3(0, 0, 0)).y;
		screenBoundaryPositions.screenOutLeft = mainCamera.ScreenToWorldPoint(new Vector3(0, 0, 0)).x;
		screenBoundaryPositions.screenOutRight = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x;
	}

	/// <summary>
	/// Returns screen boundary positions.
	/// </summary>
	/// <returns></returns>
	public ScreenBoundaryPositions GetScreenBoundaryPositions()
	{
		return screenBoundaryPositions;
	}

	/// <summary>
	/// Entry point of the game. UI button callback to setup diferent managers in order and start the game.
	/// </summary>
	public void StartGame()
	{
		CalculateScreenOutPositions();
		uiManager.EnablePlayButton(false);
		poolManager.Initialize();
		inputManager.Initialize();
		collectableManager.Initialize(this);
		playerManager.Initialize(this);

		score = 0;
		time = 0;
		if (timeCoroutine != null)
		{
			StopCoroutine(timeCoroutine);
		}
		timeCoroutine = StartCoroutine(TimeKeeper());
	}

	/// <summary>
	/// Update the score counter and reflect it on the UI.
	/// </summary>
	public void UpdateScore()
	{
		score++;
		uiManager.UpdateScoreText(score);
	}

	/// <summary>
	/// Game over callback on player tail collision.
	/// </summary>
	public void Gameover()
	{
		StopCoroutine(timeCoroutine);
		uiManager.EnableRestartButton(true);
	}

	
	/// <summary>
	/// Restart game scene on game end. 
	/// </summary>
	public void RestartGame()
	{
		SceneManager.LoadScene(0);
	}

	/// <summary>
	/// Increment time elapsed for the game using Ienumerator.
	/// </summary>
	/// <returns></returns>
	private IEnumerator TimeKeeper()
	{
		while (true)
		{
			yield return new WaitForSeconds(1);
			time++;
			uiManager.UpdateTimeText(time);
		}
	}
}
