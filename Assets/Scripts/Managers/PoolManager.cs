﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
	public GameObject tailPrefab;
	public GameObject collectiblePrefab;

	private List<GameObject> pooledTailObjects;
	private List<GameObject> pooledCollectibleObjects;

	private GameObject pooledTailObjectHierarchyParent;
	private GameObject pooledCollectibleObjectHierarchyParent;

	/// <summary>
	/// Used to initialize sub systems from main game manager in the order of requirenemnt.
	/// </summary>
	public void Initialize()
	{
		pooledTailObjects = new List<GameObject>();
		pooledCollectibleObjects = new List<GameObject>();

		SpawnTailObjects();
		SpawnCollectibleObjects();
	}


	#region Tail Object
	/// <summary>
	/// Spawn initially a bunch of tail objects to keep reusing later in the game.
	/// </summary>
	private void SpawnTailObjects()
	{
		pooledTailObjectHierarchyParent = new GameObject("Pooled Tail Objects");
		for (int i = 0; i < 150; i++)
		{
			GameObject tempObject = Instantiate(tailPrefab);
			tempObject.transform.parent = pooledTailObjectHierarchyParent.transform;
			tempObject.transform.name = "Pooled_TailObject";
			tempObject.SetActive(false);
			pooledTailObjects.Add(tempObject);
		}
	}

	/// <summary>
	/// Returns a pooled tail object to use; 
	/// </summary>
	/// <returns></returns>
	public GameObject GetTailObjectFromPooledList()
	{
		GameObject temp = null;
		temp = pooledTailObjects[0];
		pooledTailObjects.Remove(temp);
		temp.transform.name = "InUse_TailObject";
		temp.SetActive(true);
		return temp;
	}


	/// <summary>
	/// Pool back the object once removed from the tail.
	/// </summary>
	/// <param name="tailObject"></param>
	public void AddTailObjectToPooledList(GameObject tailObject)
	{
		tailObject.transform.parent = pooledTailObjectHierarchyParent.transform;
		tailObject.transform.name = "Pooled_TailObject";
		tailObject.SetActive(false);
		pooledTailObjects.Add(tailObject);
	}
	#endregion

	#region Collectible Object
	/// <summary>
	/// Spawn initially a bunch of collectable objects to keep reusing later in the game.
	/// </summary>
	private void SpawnCollectibleObjects()
	{
		pooledCollectibleObjectHierarchyParent = new GameObject("Pooled Collectible Objects");
		for (int i = 0; i < 5; i++)
		{
			GameObject tempObject = Instantiate(collectiblePrefab);
			tempObject.transform.parent = pooledCollectibleObjectHierarchyParent.transform;
			tempObject.transform.name = "Pooled_CollectibleObject";
			tempObject.SetActive(false);
			pooledCollectibleObjects.Add(tempObject);
		}
	}

	/// <summary>
	/// Returns a pooled collectable object to use; 
	/// </summary>
	/// <returns></returns>
	public GameObject GetCollectibleObjectFromPooledList()
	{
		GameObject temp = null;
		temp = pooledCollectibleObjects[0];
		pooledCollectibleObjects.Remove(temp);
		temp.transform.name = "InUse_CollectibleObject";
		temp.SetActive(true);
		return temp;
	}

	/// <summary>
	/// Pool back the object once removed from the game.
	/// </summary>
	/// <param name="collectibleObject"></param>
	public void AddCollectibleObjectToPooledList(GameObject collectibleObject)
	{
		collectibleObject.transform.parent = pooledCollectibleObjectHierarchyParent.transform;
		collectibleObject.transform.name = "Pooled_CollectableObject";
		collectibleObject.SetActive(false);
		pooledCollectibleObjects.Add(collectibleObject);
	}

	#endregion
}
