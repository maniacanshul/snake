﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleManager : MonoBehaviour
{
	private GameManager gameManager;
	private ScreenBoundaryPositions screenBoundaryPositions;

	private float topMaxPos;
	private float bottomMaxPos;
	private float leftMaxPos;
	private float rightMaxPos;

	private List<GameObject> activeCollectibleObjects;

	/// <summary>
	/// Used to initialize sub systems from main game manager in the order of requirenemnt.
	/// </summary>
	/// <param name="_gameManager"></param>
	public void Initialize(GameManager _gameManager)
	{
		gameManager = _gameManager;
		screenBoundaryPositions = gameManager.GetScreenBoundaryPositions();
		activeCollectibleObjects = new List<GameObject>();
		CalculateMaxSpawnPositions();
	}

	/// <summary>
	/// Calculate the range in which a collectable can be spawned.
	/// </summary>
	private void CalculateMaxSpawnPositions()
	{
		float distanceFromBoundary = screenBoundaryPositions.screenOutRight - screenBoundaryPositions.screenOutLeft;
		distanceFromBoundary = (distanceFromBoundary * 5) / 100;

		topMaxPos = screenBoundaryPositions.screenOutTop - distanceFromBoundary;
		bottomMaxPos = screenBoundaryPositions.screenOutBottom + distanceFromBoundary;
		leftMaxPos = screenBoundaryPositions.screenOutLeft + distanceFromBoundary;
		rightMaxPos = screenBoundaryPositions.screenOutRight - distanceFromBoundary;

		SpawnCollectible();
	}

	public void SpawnCollectible()
	{
		Vector3 tempPos = Vector3.zero;
		tempPos.x = (int)Random.Range(leftMaxPos, rightMaxPos);
		tempPos.y = (int)Random.Range(bottomMaxPos, topMaxPos);

		GameObject tempCollectible = gameManager.poolManager.GetCollectibleObjectFromPooledList();
		activeCollectibleObjects.Add(tempCollectible);

		tempCollectible.transform.position = tempPos;
	}
}
