﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementManager : MonoBehaviour
{
	public GameObject headObject;
	public float movementUpdateTime;

	private PlayerManager playerManager;

	private INPUT_DIRECTION currentDir;
	private Vector3 moveDirection;

	private Coroutine movementCoroutine;

	private ScreenBoundaryPositions screenBoundaryPositions;

	/// <summary>
	/// Player movement manager initial setup.
	/// </summary>
	/// <param name="_playerManager"></param>
	public void Initialize(PlayerManager _playerManager, ScreenBoundaryPositions boundaryPositions)
	{
		playerManager = _playerManager;
		screenBoundaryPositions = boundaryPositions;
		currentDir = INPUT_DIRECTION.UP;
		UpdateDirection(currentDir);
		movementCoroutine = null;
		movementCoroutine = StartCoroutine(MovementIE());
	}

	/// <summary>
	/// Stop player movement. Called for game over scenerio.
	/// </summary>
	public void StopPlayerMovement()
	{
		StopCoroutine(movementCoroutine);
	}

	/// <summary>
	/// Update player movement direction based on new player input recieved from input manager.
	/// </summary>
	/// <param name="dir"></param>
	public void UpdateDirection(INPUT_DIRECTION dir)
	{
		bool considerThisInput = false;
		switch (dir)
		{
			case INPUT_DIRECTION.UP:
				{
					if (currentDir != INPUT_DIRECTION.DOWN)
					{
						moveDirection = Vector2.zero;
						moveDirection.y = 1;
						considerThisInput = true;
					}
					break;
				}
			case INPUT_DIRECTION.DOWN:
				{
					if (currentDir != INPUT_DIRECTION.UP)
					{
						moveDirection = Vector2.zero;
						moveDirection.y = -1;
						considerThisInput = true;
					}
					break;
				}
			case INPUT_DIRECTION.LEFT:
				{
					if (currentDir != INPUT_DIRECTION.RIGHT)
					{
						moveDirection = Vector2.zero;
						moveDirection.x = -1;
						considerThisInput = true;
					}
					break;
				}
			case INPUT_DIRECTION.RIGHT:
				{
					if (currentDir != INPUT_DIRECTION.LEFT)
					{
						moveDirection = Vector2.zero;
						moveDirection.x = +1;
						considerThisInput = true;
					}
					break;
				}
		}

		if (considerThisInput)
		{
			currentDir = dir;
			moveDirection.x = (int)moveDirection.x;
			moveDirection.y = (int)moveDirection.y;
		}
	}

	private IEnumerator MovementIE()
	{
		while (true)
		{
			Vector3 tempHeadPos = headObject.transform.localPosition;
			headObject.transform.localPosition += moveDirection;
			playerManager.AddNewTailObjectInFrontAfterMovement(tempHeadPos);
			CheckIfSnakeOutOfView();
			yield return new WaitForEndOfFrame();
			yield return new WaitForSeconds(movementUpdateTime);
		}
	}

	/// <summary>
	/// If player is outside of camera bring it back from the other side.
	/// </summary>
	private void CheckIfSnakeOutOfView()
	{
		Vector3 tempPos = headObject.transform.position;

		if (headObject.transform.position.y > screenBoundaryPositions.screenOutTop)
		{
			tempPos.y = (int)screenBoundaryPositions.screenOutBottom;
		}
		else if (headObject.transform.position.y < screenBoundaryPositions.screenOutBottom)
		{
			tempPos.y = (int)screenBoundaryPositions.screenOutTop;
		}
		else if (headObject.transform.position.x > screenBoundaryPositions.screenOutRight)
		{
			tempPos.x = (int)screenBoundaryPositions.screenOutLeft;
		}
		else if (headObject.transform.position.x < screenBoundaryPositions.screenOutLeft)
		{
			tempPos.x = (int)screenBoundaryPositions.screenOutRight;
		}

		headObject.transform.position = tempPos;
	}
}
