﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionManager : MonoBehaviour
{
	private PlayerManager playerManager;

	/// <summary>
	/// Player collision manager initial setup.
	/// </summary>
	/// <param name="_playerManager"></param>
	public void Initialize(PlayerManager _playerManager)
	{
		playerManager = _playerManager;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Tail"))
		{
			playerManager.PlayerCollisionWithTail();
		}
		else if (collision.CompareTag("Collectible"))
		{
			playerManager.PlayerCollectedACollectible(collision.gameObject);
		}
	}
}
