﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
	public int initialTailObjects;

	private GameManager gameManager;
	private PlayerMovementManager movementManager;
	private PlayerCollisionManager collisionManager;

	private List<GameObject> activeTailObject;

	private bool collectedACollectible;

	private void Awake()
	{
		movementManager = GetComponent<PlayerMovementManager>();
		collisionManager = GetComponent<PlayerCollisionManager>();
	}

	/// <summary>
	/// Used to initialize sub systems from main game manager in the order of requirenemnt.
	/// </summary>
	/// <param name="_gameManager"></param>
	public void Initialize(GameManager _gameManager)
	{
		collectedACollectible = false;
		gameManager = _gameManager;
		gameManager.inputManager.OnInputRecievedEvent += movementManager.UpdateDirection;
		activeTailObject = new List<GameObject>();
		BuildInitialSnake();
		collisionManager.Initialize(this);
		movementManager.Initialize(this, gameManager.GetScreenBoundaryPositions());
	}

	/// <summary>
	/// Build initial snake by adding "initialTailObjects" number of tail objects;
	/// </summary>
	private void BuildInitialSnake()
	{
		Vector3 tempPos = movementManager.headObject.transform.localPosition;

		for (int i = 0; i < initialTailObjects; i++)
		{
			tempPos.y -= 1;
			AddNewTailObjectInFrontAfterMovement(tempPos, false);
		}
	}

	/// <summary>
	/// Add a new tail object in the position of head object before movement.
	/// </summary>
	/// <param name="headPosBeforeMovement">Head position before movement.</param>
	public void AddNewTailObjectInFrontAfterMovement(Vector3 headPosBeforeMovement, bool removeTail = true)
	{
		GameObject newTail = gameManager.poolManager.GetTailObjectFromPooledList();
		newTail.transform.parent = transform;
		newTail.transform.localPosition = headPosBeforeMovement;
		newTail.transform.localScale = Vector3.one;
		activeTailObject.Add(newTail);

		if (removeTail && !collectedACollectible)
		{
			RemoveLastTailObjectAfterMovement();
		}
		else if (collectedACollectible)
		{
			collectedACollectible = false;
		}
	}

	/// <summary>
	/// Remove last tail object after the movement is done.
	/// </summary>
	/// <param name="tailObject">Tail object to remove.</param>
	private void RemoveLastTailObjectAfterMovement()
	{
		//Do not add when eating a new object.
		gameManager.poolManager.AddTailObjectToPooledList(activeTailObject[0]);
		activeTailObject.Remove(activeTailObject[0]);
	}


	/// <summary>
	/// Callback when the player collects a collectible.
	/// </summary>
	public void PlayerCollectedACollectible(GameObject collectibleObject)
	{
		collectedACollectible = true;
		gameManager.collectableManager.SpawnCollectible();
		gameManager.poolManager.AddCollectibleObjectToPooledList(collectibleObject);
		gameManager.UpdateScore();
	}


	/// <summary>
	/// Player head collided with tail.
	/// </summary>
	public void PlayerCollisionWithTail()
	{
		movementManager.StopPlayerMovement();
		gameManager.Gameover();
	}
}
