﻿//File containing all th enums used in the project.


public enum INPUT_DIRECTION
{
	NONE, UP, DOWN, LEFT, RIGHT
}

public enum GAME_STATE
{
	MENU, PAUSE, PLAY
}